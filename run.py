
#  Commad format
#   ui_in
#   x x x x  x x x x
#     ^^^^^^^^^^^^^^
#     Addr 1
#   ^ Mode
# 
#   uio_in
#   x x x x  x x x x
#     ^^^^^^^^^^^^^^
#     Addr 2
#   ^ Commit
# 
#  The circuit operates in two modes.
#   In mode 0:
#       Addr 1 specifies the read index for opa, and addr2 is unused. No writeback
#       is performed
#   In mode 1:
#       Addr 1 specifies the read index for opb, and addr2 specifies the writeback address.
#       On the rising edge of commit, the output of the nand gate is written to
#       the writeback address

def do_nand_inner(opa_addr, opb_addr, dest, push_output: bool) -> list[str]:
    commands = []
    # # Mode 0, se opa
    # commands.append(f"tt.input_byte = {((0 << 7) | opa_addr)} # opa addr") #addr1
    # # Set output
    # commands.append(f"tt.bidir_byte = {(1 if push_output else 0)} # push output")


    # commands.append(f"tt.input_byte = {((1 << 7) | opb_addr)} # opb addr")
    # commands.append(f"tt.bidir_byte = {((0 << 7) | dest)} # set dest but don't commit")


    # commands.append(f"tt.input_byte = {((1 << 7) | opb_addr)}")
    # commands.append(f"tt.bidir_byte = {((1 << 7) | dest)} # commit") # commit

    # commands.append(f"tt.bidir_byte = {((0 << 7) | dest)} # deassert") # deassert commit again
    # commands.append("")

    commands = [
        # Enter opa and dest
        {"mode": False, "opa_or_opb": opa_addr, "write_out_or_dest": 1 if "push_output" else 0, "commit": False},
        # Switch to mode 1 while retaining output
        {"mode": True, "opa_or_opb": opa_addr, "write_out_or_dest": 1 if "push_output" else 0, "commit": False},
        # Switch to the actual mode 1 output
        {"mode": True, "opa_or_opb": opb_addr, "write_out_or_dest": dest, "commit": False},
        # Commit
        {"mode": True, "opa_or_opb": opb_addr, "write_out_or_dest": dest, "commit": True},
        # Uncommit
        {"mode": True, "opa_or_opb": opb_addr, "write_out_or_dest": dest, "commit": False},
        {"mode": False, "opa_or_opb": opb_addr, "write_out_or_dest": dest, "commit": False},
    ]

    result = []
    for command in commands:
        mode = 1 if command["mode"] else 0
        commit = 1 if command["commit"] else 0
        result.append(f"tt.input_byte = 0b{((mode << 7) | command["opa_or_opb"]):08b}")
        result.append(f"tt.bidir_byte = 0b{((commit << 7) | command["write_out_or_dest"]):08b}")

    result.append("")

    return result




def main():
    commands = list(map(str, """
import time
from machine import Pin
from ttboard.mode import RPMode
from ttboard.demoboard import DemoBoard

# get a handle to the board
tt = DemoBoard()

# enable a specific project, e.g.
tt.shuttle.tt_um_thezoq2_tmng.enable()
tt.mode = RPMode.ASIC_RP_CONTROL

tt.reset_project(True)
tt.reset_project(False)
tt.clock_project_PWM(2e6) # clocking projects @ 2MHz
    """.split("\n")))

    for i in range(0, 8):
        commands.append(f"tt.uio{i}.mode = Pin.OUT")

    # Fabricate a 1 into position 0
    commands.append("for i in range(0, 100):")
    commands.append("    time.sleep_ms(1000)")
    # for _ in range(0, 9):
    #     # Output a 0 on all outputs
    #     commands.extend(list(map(lambda x: "    " + x, do_nand_inner(2, 2, 2, True))))
    
    commands.extend(list(map(lambda x: "    " + x, do_nand_inner(1, 1, 127, False))))
    commands.extend(list(map(lambda x: "    " + x, do_nand_inner(1, 1, 126, False))))

    commands.extend(list(map(lambda x: "    " + x, do_nand_inner(0, 0, 127, False))))
    commands.extend(list(map(lambda x: "    " + x, do_nand_inner(0, 0, 126, False))))
    commands.extend(list(map(lambda x: "    " + x, do_nand_inner(0, 0, 125, False))))
    commands.extend(list(map(lambda x: "    " + x, do_nand_inner(0, 0, 124, False))))

    commands.extend(list(map(lambda x: "    " + x, do_nand_inner(0, 0, 127, False))))
    commands.extend(list(map(lambda x: "    " + x, do_nand_inner(0, 0, 127, False))))
    commands.extend(list(map(lambda x: "    " + x, do_nand_inner(0, 0, 127, False))))
    commands.extend(list(map(lambda x: "    " + x, do_nand_inner(0, 0, 127, False))))

    commands.extend(list(map(lambda x: "    " + x, do_nand_inner(1, 127, 126, True))))
    commands.extend(list(map(lambda x: "    " + x, do_nand_inner(1, 125, 124, True))))
    commands.extend(list(map(lambda x: "    " + x, do_nand_inner(1, 126, 127, True))))
    commands.extend(list(map(lambda x: "    " + x, do_nand_inner(1, 124, 126, True))))

    commands.extend(list(map(lambda x: "    " + x, do_nand_inner(1, 127, 126, True))))
    commands.extend(list(map(lambda x: "    " + x, do_nand_inner(1, 125, 124, True))))
    commands.extend(list(map(lambda x: "    " + x, do_nand_inner(1, 126, 127, True))))
    commands.extend(list(map(lambda x: "    " + x, do_nand_inner(1, 124, 126, True))))

    for command in commands:
        print(command)

if __name__ == "__main__":
    main()

