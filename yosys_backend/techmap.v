// (* techmap_celltype = "NOT" *)
// module NAND_NOT (input A, output Y);
// 	NAND u1 (.A(A), .B(A), .Y(Y));
// endmodule
// 
// (* techmap_celltype = "BUF" *)
// module NAND_BUF (input A, output Y);
// 	wire out1;
// 	NOT u1 (.A(A), .Y(out1));
// 	NOT u2 (.A(A), .Y(Y));
// endmodule

