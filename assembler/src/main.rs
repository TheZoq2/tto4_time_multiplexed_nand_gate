use std::{
    collections::HashMap,
    path::{Path, PathBuf},
};

use clap::{Parser, Subcommand};
use color_eyre::{
    eyre::{anyhow, bail, Context},
    Result,
};
use regex::Regex;

#[derive(Subcommand)]
enum Target {
    Spade,
    Python,
}
#[derive(Parser)]
struct Args {
    #[command(subcommand)]
    target: Target,
}

#[derive(Debug)]
struct RawCommand {
    opa_addr: u32,
    opb_addr: u32,
    dest_addr: u32,
    push_output: bool,
}

impl RawCommand {
    pub fn encode(&self) -> Vec<String> {
        vec![
            format!(
                "(0b{:08b}u, 0b{:08b}u),",
                self.opa_addr,
                if self.push_output { 1 } else { 0 }
            ),
            format!(
                "(0b{:08b}u, 0b{:08b}u),",
                (1 << 7) | self.opa_addr,
                if self.push_output { 1 } else { 0 }
            ),
            format!(
                "(0b{:08b}u, 0b{:08b}u),",
                (1 << 7) | self.opb_addr,
                (0 << 7) | self.dest_addr
            ),
            format!(
                "(0b{:08b}u, 0b{:08b}u),",
                (1 << 7) | self.opb_addr,
                (1 << 7) | self.dest_addr
            ),
            format!(
                "(0b{:08b}u, 0b{:08b}u),",
                (1 << 7) | self.opb_addr,
                (0 << 7) | self.dest_addr
            ),
        ]
    }

    pub fn encode_python(&self) -> Vec<String> {
        vec![
            format!(
                "(0b{:08b}, 0b{:08b}),",
                self.opa_addr,
                if self.push_output { 1 } else { 0 }
            ),
            format!(
                "(0b{:08b}, 0b{:08b}),",
                (1 << 7) | self.opa_addr,
                if self.push_output { 1 } else { 0 }
            ),
            format!(
                "(0b{:08b}, 0b{:08b}),",
                (1 << 7) | self.opb_addr,
                (0 << 7) | self.dest_addr
            ),
            format!(
                "(0b{:08b}, 0b{:08b}),",
                (1 << 7) | self.opb_addr,
                (1 << 7) | self.dest_addr
            ),
            format!(
                "(0b{:08b}, 0b{:08b}),",
                (1 << 7) | self.opb_addr,
                (0 << 7) | self.dest_addr
            ),
        ]
    }
}

#[derive(Clone, Debug)]
enum Operand {
    Int(u32),
    Var(String),
}

impl Operand {
    fn parse(s: &str) -> Option<Self> {
        if s.starts_with("$") {
            Some(Operand::Var(s.to_string()))
        } else {
            s.parse::<u32>().ok().map(Operand::Int)
        }
    }

    fn lower(&self) -> Result<u32> {
        match self {
            Operand::Int(v) => {
                if *v > 127 {
                    bail!("Operand idx > 127")
                }
                Ok(*v)
            }
            Operand::Var(v) => Err(anyhow!("Unlowered variable {v}")),
        }
    }
}

#[derive(Clone, Debug)]
enum Op {
    Nand,
    Inv,
    Copy,
}

#[derive(Clone, Debug)]
struct Insn {
    op: Op,
    operands: Vec<Operand>,
    scratchpads: Vec<Operand>,
    dest: Operand,
    push_out: bool,
    line: usize,
}

impl Insn {
    fn two_nand(a: Operand, b: Operand, dest: Operand, push_out: bool) -> Self {
        Self {
            op: Op::Nand,
            operands: vec![a, b],
            scratchpads: vec![],
            dest,
            push_out,
            line: 0,
        }
    }

    fn lower(self) -> Result<Vec<Self>> {
        let line = self.line;
        self.lower_inner()
            .with_context(|| format!("On line {line}"))
    }

    fn lower_inner(self) -> Result<Vec<Self>> {
        match (&self.op, self.operands.as_slice()) {
            (Op::Nand, []) => {
                bail!("nand without input operands")
            }
            (Op::Nand, [_]) => {
                bail!("nand with single operand")
            }
            (Op::Nand, [_, _]) => {
                if !self.scratchpads.is_empty() {
                    bail!("Double nand with scratchpads")
                } else {
                    Ok(vec![self.clone()])
                }
            }
            (Op::Nand, [x, y, z]) => {
                if !self.scratchpads.is_empty() {
                    bail!("Triple nand with scratchpads")
                } else {
                    Ok(vec![
                        Self::two_nand(x.clone(), y.clone(), self.dest.clone(), false),
                        Self::two_nand(
                            self.dest.clone(),
                            self.dest.clone(),
                            self.dest.clone(),
                            false,
                        ),
                        Self::two_nand(
                            self.dest.clone(),
                            z.clone(),
                            self.dest.clone(),
                            self.push_out,
                        ),
                    ])
                }
            }
            (Op::Nand, [x, y, z, w]) => {
                if self.scratchpads.len() != 1 {
                    bail!("Quad nand requires a scratchpad")
                } else {
                    Ok(vec![
                        Self::two_nand(x.clone(), y.clone(), self.scratchpads[0].clone(), false),
                        Self::two_nand(
                            self.scratchpads[0].clone(),
                            self.scratchpads[0].clone(),
                            self.scratchpads[0].clone(),
                            false,
                        ),
                        Self::two_nand(z.clone(), w.clone(), self.dest.clone(), false),
                        Self::two_nand(
                            self.dest.clone(),
                            self.dest.clone(),
                            self.dest.clone(),
                            false,
                        ),
                        Self::two_nand(
                            self.dest.clone(),
                            self.scratchpads[0].clone(),
                            self.dest.clone(),
                            self.push_out,
                        ),
                    ])
                }
            }
            (Op::Nand, other) => {
                bail!("{} is too many operands for nand", other.len())
            }
            (Op::Inv, [op]) => {
                if !self.scratchpads.is_empty() {
                    bail!("Inverter should not have a scratchpad")
                } else {
                    Ok(vec![Self::two_nand(
                        op.clone(),
                        op.clone(),
                        self.dest.clone(),
                        self.push_out,
                    )])
                }
            }
            (Op::Inv, _) => {
                bail!("Only single input inverters are possible")
            }
            (Op::Copy, [op]) => {
                if !self.scratchpads.is_empty() {
                    bail!("Copy should not have a scratchpad")
                } else {
                    Ok(vec![
                        Self::two_nand(op.clone(), op.clone(), self.dest.clone(), false),
                        Self::two_nand(
                            self.dest.clone(),
                            self.dest.clone(),
                            self.dest.clone(),
                            self.push_out,
                        ),
                    ])
                }
            }
            (Op::Copy, _) => {
                bail!("Only single input copy operations possible")
            }
        }
    }

    fn compile(&self) -> Result<RawCommand> {
        match self.op {
            Op::Nand => {
                if !self.scratchpads.is_empty() || self.operands.len() != 2 {
                    bail!(
                        "{} Found unlowered gate with {} operands and {} and scratchpads",
                        self.line,
                        self.operands.len(),
                        self.scratchpads.len()
                    )
                } else {
                    Ok(RawCommand {
                        opa_addr: self.operands[0].lower()?,
                        opb_addr: self.operands[1].lower()?,
                        dest_addr: self.dest.lower()?,
                        push_output: self.push_out,
                    })
                }
            }
            Op::Inv => {
                bail!("{} Inverter should have been lowered", self.line)
            }
            Op::Copy => {
                bail!("{} Inverter should have been lowered", self.line)
            }
        }
    }
}

fn parse_line(lineno: usize, line: &str) -> Result<Option<Insn>> {
    let line = line.trim();
    if line.is_empty() || line.starts_with("//") {
        return Ok(None);
    }

    let mut split = line.split_whitespace().peekable();

    let is_output = if split.peek() == Some(&"out") {
        split.next();
        true
    } else {
        false
    };

    let op = match split.next() {
        Some("&") => Op::Nand,
        Some("!") => Op::Inv,
        Some("*") => Op::Copy,
        Some(op) => bail!("line {lineno}: Unknown op {op}"),
        None => bail!("line {lineno} has no op"),
    };

    let mut operands = vec![];
    while let Some(operand) = split.peek().and_then(|s| Operand::parse(s)) {
        split.next().unwrap();
        operands.push(operand);
    }

    let scratchpads = if split.peek() == Some(&":") {
        split.next().unwrap();
        let mut result = vec![];

        while let Some(operand) = split.peek().and_then(|s| Operand::parse(s)) {
            split.next().unwrap();
            result.push(operand)
        }
        result
    } else {
        vec![]
    };

    match split.next() {
        Some("->") => {}
        other => bail!("Expected -> got {other:?}"),
    };

    let dest = split
        .next()
        .ok_or_else(|| anyhow!("Expected output operand"))
        .and_then(|s| Operand::parse(s).ok_or_else(|| anyhow!("Expected operand. got {s:?}")))?;

    Ok(Some(Insn {
        op,
        operands,
        scratchpads,
        dest,
        push_out: is_output,
        line: lineno,
    }))
}

fn parse_blif(
    file: &Path,
    input_map: HashMap<String, usize>,
    output_map: HashMap<String, usize>,
    mut index_base: usize,
) -> Result<Vec<String>> {
    let content = std::fs::read_to_string(file).context(format!("Failed to read {file:?}"))?;

    let re = Regex::new(r"\.subckt NAND A=(.*) B=(.*) Y=(.*)")?;

    let mut name_map = HashMap::new();
    let mut lookup_or_add = move |var: &str| {
        let out = match (
            input_map.get(var),
            output_map.get(var),
            name_map.get(var).clone(),
        ) {
            (Some(v), None, None) => *v,
            (None, Some(v), None) => *v,
            (None, None, Some(v)) => *v,
            (None, None, None) => {
                let id = index_base;
                index_base += 1;
                name_map.insert(var.to_string(), id);
                id
            }
            other => bail!("Multiple mappings for {var} {other:?}"),
        };
        Ok(out as u32)
    };
    content
        .lines()
        .filter_map(|line| {
            re.captures(line).map(|cap| {
                let a = lookup_or_add(cap.get(1).unwrap().as_str())?;
                let b = lookup_or_add(cap.get(2).unwrap().as_str())?;
                let y = lookup_or_add(cap.get(3).unwrap().as_str())?;

                Ok(format!("& {a} {b} -> {y}"))
            })
        })
        .collect()
}

#[allow(dead_code)]
fn tripple_nand_test() -> String {
    "
    out & 0 0 0 -> 2
    out & 0 0 1 -> 2
    out & 0 1 0 -> 2
    out & 0 1 1 -> 2
    out & 1 0 0 -> 2
    out & 1 0 1 -> 2
    out & 1 1 0 -> 2
    out & 1 1 1 -> 2
    "
    .to_string()
}

#[allow(dead_code)]
fn quad_nand_test() -> String {
    r#"
        out & 1 0 0 0 : 3 -> 2
        out & 1 0 0 1 : 3 -> 2
        out & 1 0 1 0 : 3 -> 2
        out & 1 0 1 1 : 3 -> 2
        out & 1 1 0 0 : 3 -> 2
        out & 1 1 0 1 : 3 -> 2
        out & 1 1 1 0 : 3 -> 2
        out & 1 1 1 1 : 3 -> 2
    "#
    .to_string()
}

#[allow(dead_code)]
fn sum(x: i32, y: i32, c: i32, sum_out: i32) -> String {
    let code = r#"
        ! x -> xI
        ! y -> yI
        ! c -> cI
        & c xI yI -> a1
        & cI xI y -> a2
        & c x y -> a3
        & cI x yI -> a4
        out & a1 a2 a3 a4 : 127 -> sum_out
    "#;

    code.replace("xI", "20")
        .replace("yI", "21")
        .replace("cI", "22")
        .replace("x", &format!("{x}"))
        .replace("y", &format!("{y}"))
        .replace("c", &format!("{c}"))
        .replace("sum_out", &format!("{sum_out}"))
        .replace("a1", "31")
        .replace("a2", "32")
        .replace("a3", "33")
        .replace("a4", "34")
}

#[allow(dead_code)]
fn full_adder(x: i32, y: i32, c: i32, sum_out: i32, carry_out: i32) -> String {
    let code = r#"
        ! x -> xI
        ! y -> yI
        ! c -> cI
        & c xI yI -> a1
        & cI xI y -> a2
        & c x y -> a3
        & cI x yI -> a4
        & a1 a2 a3 a4 : 127 -> sum_out
        // a1.. are now stale, we can re-use them
        & x y -> a1
        & c x -> a2
        & c y -> a3
        & a1 a2 a3 -> carry_out
    "#;

    code.replace("sum_out", &format!("{sum_out}"))
        .replace("carry_out", &format!("{carry_out}"))
        .replace("xI", "50")
        .replace("yI", "51")
        .replace("cI", "52")
        .replace("x", &format!("{x}"))
        .replace("y", &format!("{y}"))
        .replace("c", &format!("{c}"))
        .replace("a1", "61")
        .replace("a2", "62")
        .replace("a3", "63")
        .replace("a4", "64")
}

fn main() -> Result<()> {
    let spade_template = include_str!("./template.spade");
    let python_template = include_str!("./template.py");

    // let input = quad_nand_test();
    // let input = vec![
    //     full_adder(0, 0, 0, 10, 20),
    //     full_adder(0, 0, 1, 11, 21),
    //     full_adder(0, 1, 0, 12, 22),
    //     full_adder(0, 1, 1, 13, 23),
    //     full_adder(1, 0, 0, 14, 24),
    //     full_adder(1, 0, 1, 15, 25),
    //     full_adder(1, 1, 0, 16, 26),
    //     full_adder(1, 1, 1, 17, 27),
    //     "
    //     out ! 10 -> 0
    //     out ! 11 -> 0
    //     out ! 12 -> 0
    //     out ! 13 -> 0
    //     out ! 14 -> 0
    //     out ! 15 -> 0
    //     out ! 16 -> 0
    //     out ! 17 -> 0
    //     ".to_string()
    // ].join("\n");

    let sevenseg = parse_blif(
        &PathBuf::from("../yosys_backend/build/blif.blif"),
        [
            ("in_i[3]", 34),
            ("in_i[2]", 33),
            ("in_i[1]", 32),
            ("in_i[0]", 31),
        ]
        .iter()
        .map(|(k, v)| (k.to_string(), *v))
        .collect(),
        [
            ("output__[0]", 40),
            ("output__[1]", 41),
            ("output__[2]", 42),
            ("output__[3]", 43),
            ("output__[4]", 44),
            ("output__[5]", 45),
            ("output__[6]", 46),
        ]
        .iter()
        .map(|(k, v)| (k.to_string(), *v))
        .collect(),
        50,
    )?
    .join("\n");

    println!("{sevenseg}");

    let input = vec![
        "! 0 -> 20".to_string(), // Init carry to 0
        full_adder(10, 0, 20, 30, 20),
        full_adder(11, 0, 20, 31, 20),
        full_adder(12, 0, 20, 32, 20),
        full_adder(13, 0, 20, 33, 20),
        full_adder(14, 0, 20, 34, 20),
        full_adder(15, 0, 20, 35, 20),
        full_adder(16, 0, 20, 36, 20),
        full_adder(17, 0, 20, 37, 20),
        sevenseg,
        "
        * 30 -> 10
        * 31 -> 11
        * 32 -> 12
        * 33 -> 13
        * 34 -> 14
        * 35 -> 15
        * 36 -> 16
        * 37 -> 17

        out * 30 -> 30
        out * 40 -> 40
        out * 41 -> 41
        out * 42 -> 42
        out * 43 -> 43
        out * 44 -> 44
        out * 45 -> 45
        out * 46 -> 46
        // out ! 0 -> 0
        // out ! 0 -> 0
        // out ! 0 -> 0
        // out ! 0 -> 0
        "
        .to_string(),
    ]
    .join("\n");

    // let input = vec![
    //     // "! 1 -> 20".to_string(), // Init carry to 0
    //     // full_adder(10, 0, 20, 30, 20),
    //     // full_adder(11, 0, 20, 31, 20),
    //     // full_adder(12, 0, 20, 32, 20),
    //     "
    //     ! 1 -> 127
    //     ! 1 -> 126

    //     ! 0 -> 127
    //     ! 0 -> 126
    //     ! 0 -> 125
    //     ! 0 -> 124

    //     ! 0 -> 127
    //     ! 0 -> 127
    //     ! 0 -> 127
    //     ! 0 -> 127
    //     out ! 127 -> 126
    //     out ! 125 -> 124
    //     out ! 126 -> 127
    //     out ! 124 -> 126

    //     out ! 127 -> 126
    //     out ! 125 -> 124
    //     out ! 126 -> 127
    //     out ! 124 -> 126
    //     // out * 31 -> 11
    //     // out * 32 -> 12
    //     // out * 33 -> 13
    //     // out * 34 -> 14
    //     // out * 35 -> 15
    //     // out * 36 -> 16
    //     // out * 37 -> 17
    //     // out ! 0 -> 0
    //     // out ! 0 -> 0
    //     // out ! 0 -> 0
    //     // out ! 0 -> 0
    //     "
    //     .to_string(),
    // ]
    // .join("\n");

    let mut insns = vec![];
    for (n, line) in input.lines().enumerate() {
        let mut this_insns = parse_line(n, line)?
            .map(|insn| insn.lower())
            .transpose()?
            .unwrap_or_default();

        insns.append(&mut this_insns)
    }
    // println!("{insns:?}");

    let args = Args::parse();

    let compiled = insns
        .iter()
        .map(|i| i.compile())
        .collect::<Result<Vec<_>>>()?;

    match args.target {
        Target::Spade => {
            let commands = compiled
                .into_iter()
                .map(|c| c.encode())
                .flatten()
                .collect::<Vec<_>>();

            let commands_str = commands.iter().cloned().collect::<Vec<_>>().join("\n");

            let result = spade_template
                .replace("$COMMANDS", &commands_str)
                .replace("$MAX_IDX", &format!("{}", commands.len() - 1));

            println!("{result}");

            std::fs::write("../src/generated_driver.spade", result)?;
        }
        Target::Python => {
            let commands = compiled
                .into_iter()
                .map(|c| c.encode_python())
                .flatten()
                .collect::<Vec<_>>();

            let commands_str = commands.iter().cloned().collect::<Vec<_>>().join("\n");

            let result = python_template
                .replace("$COMMANDS", &commands_str)
                .replace("$MAX_IDX", &format!("{}", commands.len() - 1));

            std::fs::write("../generated.py", result)?;
        }
    }

    Ok(())
}
