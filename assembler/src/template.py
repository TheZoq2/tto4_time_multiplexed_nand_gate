
import time
from machine import Pin
from ttboard.mode import RPMode
from ttboard.demoboard import DemoBoard

# get a handle to the board
tt = DemoBoard()

# enable a specific project, e.g.
tt.shuttle.tt_um_thezoq2_tmng.enable()
tt.mode = RPMode.ASIC_RP_CONTROL

tt.reset_project(True)
tt.reset_project(False)
tt.clock_project_PWM(2e6) # clocking projects @ 2MHz
tt.bidir_mode = 0xff
        
tt.uio0.mode = Pin.OUT
tt.uio1.mode = Pin.OUT
tt.uio2.mode = Pin.OUT
tt.uio3.mode = Pin.OUT
tt.uio4.mode = Pin.OUT
tt.uio5.mode = Pin.OUT
tt.uio6.mode = Pin.OUT
tt.uio7.mode = Pin.OUT

commands = [
    $COMMANDS
]

while True:
    time.sleep_ms(1000)
    for (ui, uio) in commands:
        tt.input_byte = ui; tt.bidir_byte = uio

