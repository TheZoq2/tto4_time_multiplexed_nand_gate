#top=main::top_impl

import cocotb
from cocotb.clock import Clock
from cocotb.triggers import FallingEdge
from spade import SpadeExt

async def do_nand_inner(clk, s, opa_addr, opb_addr, dest, push_output: bool):
    print(f"{opa_addr} 󰣢 {opb_addr} -> {dest}")
    s.i.ui_in = f"{((0 << 7) | opa_addr)}u" #addr1
    s.i.uio_in = f"{(1 if push_output else 0)}u"

    await FallingEdge(clk)

    s.i.ui_in = f"{((1 << 7) | opb_addr)}u"
    s.i.uio_in = f"{((0 << 7) | dest)}u" # Set address but don't commit

    await FallingEdge(clk)
    s.i.ui_in = f"{((1 << 7) | opb_addr)}u"
    s.i.uio_in = f"{((1 << 7) | dest)}u" # commit

    # Stay committed for a cycle to ensure we don't double write
    await FallingEdge(clk)

    # Uncommit
    await FallingEdge(clk)
    s.i.ui_in = f"{((1 << 7) | opb_addr)}u"
    s.i.uio_in = f"{((0 << 7) | dest)}u" # deassert commit again
    await FallingEdge(clk)

async def do_nand(clk, s, opa_addr, opb_addr, dest):
    await do_nand_inner(clk, s, opa_addr, opb_addr, dest, False)


async def do_nand_push(clk, s, opa_addr, opb_addr, dest):
    await do_nand_inner(clk, s, opa_addr, opb_addr, dest, True)

async def invert(clk, s, source, dest):
    await do_nand(clk, s, source, source, dest)

# Tripple input nand, uses `dest` as a scratchpad register until done
async def tri_nand(clk, s, addr1, addr2, addr3, dest):
    await do_nand(clk, s, addr1, addr2, dest)
    await invert(clk, s, dest, dest)
    await do_nand(clk, s, dest, addr3, dest)


async def quad_nand(clk, s, addr1, addr2, addr3, addr4, dest, buff):
    await do_nand(clk, s, addr1, addr2, dest)
    await invert(clk, s, dest, dest)
    await do_nand(clk, s, addr3, addr4, buff)
    await invert(clk, s, buff, buff)
    await do_nand(clk, s, dest, buff, dest)

async def test_constant(dut, a, b, expected):
    s = SpadeExt(dut)

    clk = dut.clk

    await cocotb.start(Clock(clk, 10).start())
    await FallingEdge(clk)

    await do_nand(clk, s, a, b, 0) # 0 is a scratchpad
    s.o.nand_snoop.assert_eq(f"{'true' if expected else 'false'}")

@cocotb.test()
async def const_00_is_1(dut):
    await test_constant(dut, 0, 0, 1)


@cocotb.test()
async def const_01_is_1(dut):
    await test_constant(dut, 0, 1, 1)


@cocotb.test()
async def const_10_is_1(dut):
    await test_constant(dut, 1, 0, 1)


@cocotb.test()
async def const_11_is_1(dut):
    await test_constant(dut, 1, 1, 0)

@cocotb.test()
async def inverter_works(dut):
    s = SpadeExt(dut)

    clk = dut.clk

    await cocotb.start(Clock(clk, 10).start())
    await FallingEdge(clk)

    await invert(clk, s, 0, 2)
    s.o.nand_snoop.assert_eq("true")


    await invert(clk, s, 1, 2)
    s.o.nand_snoop.assert_eq("false")

@cocotb.test()
async def tri_nand_works(dut):
    s = SpadeExt(dut)

    clk = dut.clk

    await cocotb.start(Clock(clk, 10).start())
    await FallingEdge(clk)

    async def test_input(x, y, z, out):
        await tri_nand(clk, s, x, y, z, 127)
        s.o.nand_snoop.assert_eq(f"{'true' if out else 'false'}")

    await test_input(0, 0, 0, 1)
    await test_input(0, 0, 1, 1)
    await test_input(0, 1, 0, 1)
    await test_input(0, 1, 1, 1)
    await test_input(1, 0, 0, 1)
    await test_input(1, 0, 1, 1)
    await test_input(1, 1, 0, 1)
    await test_input(1, 1, 1, 0)


@cocotb.test()
async def quad_nand_works(dut):
    s = SpadeExt(dut)

    clk = dut.clk

    await cocotb.start(Clock(clk, 10).start())
    await FallingEdge(clk)

    async def test_input(x, y, z, w, out):
        await quad_nand(clk, s, x, y, z, w, 127, 126)
        s.o.nand_snoop.assert_eq(f"{'true' if out else 'false'}")

    await test_input(0, 0, 0, 0, 1)
    await test_input(0, 0, 0, 1, 1)
    await test_input(0, 0, 1, 0, 1)
    await test_input(0, 0, 1, 1, 1)
    await test_input(0, 1, 0, 0, 1)
    await test_input(0, 1, 0, 1, 1)
    await test_input(0, 1, 1, 0, 1)
    await test_input(0, 1, 1, 1, 1)
    await test_input(1, 0, 0, 0, 1)
    await test_input(1, 0, 0, 1, 1)
    await test_input(1, 0, 1, 0, 1)
    await test_input(1, 0, 1, 1, 1)
    await test_input(1, 1, 0, 0, 1)
    await test_input(1, 1, 0, 1, 1)
    await test_input(1, 1, 1, 0, 1)
    await test_input(1, 1, 1, 1, 0)

@cocotb.test()
async def output_buffer_works(dut):
    s = SpadeExt(dut)

    clk = dut.clk

    await cocotb.start(Clock(clk, 10).start())
    await FallingEdge(clk)

    # Fill the output buffer with 0
    for _ in range(0, 8):
        await do_nand_push(clk, s, 1, 1, 0)

    s.o.output_bits.assert_eq("0")

    # Push a 1
    await do_nand_push(clk, s, 0, 0, 0)
    s.o.output_bits.assert_eq("1")

    # Then a 0 ensuring that the 1 has moved over
    await do_nand_push(clk, s, 1, 1, 0)
    s.o.output_bits.assert_eq("0b10")

@cocotb.test()
async def repeated_inverter_works(dut):
    s = SpadeExt(dut)

    clk = dut.clk

    await cocotb.start(Clock(clk, 10).start())
    await FallingEdge(clk)

    await invert(clk, s, 1, 2) # Write 0 to 2
    await invert(clk, s, 2, 2) # Invert 2
    s.o.nand_snoop.assert_eq("true")
    await invert(clk, s, 2, 2) # Invert 2
    s.o.nand_snoop.assert_eq("false")
    await invert(clk, s, 2, 2) # Invert 2
    s.o.nand_snoop.assert_eq("true")
    await invert(clk, s, 2, 2) # Invert 2
    s.o.nand_snoop.assert_eq("false")
