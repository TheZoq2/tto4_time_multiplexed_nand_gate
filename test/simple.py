#top=main::top_impl

from cocotb.clock import Clock
from cocotb.triggers import FallingEdge
from spade import *

import cocotb

async def perform_nand(clk, s: SpadeExt, opa: int, opb: int, wb: int):
    # Mode 0
    s.i.ui_in = f"{(0 << 7) | opa}u"
    # No commit, address is ignored
    s.i.uio_in = f"{(0 << 7) | 0}"

    await FallingEdge(clk)

    # Mode 1
    s.i.ui_in = f"{(1 << 7) | opb}u"
    # Still no commit, but wb should be set
    s.i.uio_in = f"{(0 << 7) | wb}u"

    await FallingEdge(clk)

    # Commit
    s.i.uio_in = f"{(1 << 7) | wb}u"
    await FallingEdge(clk)

    # Uncommit
    s.i.uio_in = f"{(1 << 7) | wb}u"
    await FallingEdge(clk)

async def test_constants(dut, opa, opb, expected):
    s = SpadeExt(dut)

    clk = dut.clk

    await cocotb.start(Clock(clk, 10).start())
    await FallingEdge(clk)

    await perform_nand(clk, s, opa, opb, 2)
    s.o.nand_snoop.assert_eq(f"{'true' if expected else 'false'}")


@cocotb.test()
async def const_00_works(dut):
    await test_constants(dut, 0, 0, 1)


@cocotb.test()
async def const_01_works(dut):
    await test_constants(dut, 0, 1, 1)


@cocotb.test()
async def const_10_works(dut):
    await test_constants(dut, 0, 1, 1)


@cocotb.test()
async def const_11_works(dut):
    await test_constants(dut, 1, 1, 0)


@cocotb.test()
async def regfile_read_store_works(dut):
    s = SpadeExt(dut)

    clk = dut.clk

    await cocotb.start(Clock(clk, 10).start())
    await FallingEdge(clk)

    # Write 0 to register 2
    await perform_nand(clk, s, 1, 1, 2)

    # Then read register 2. Write to register 0 as /dev/null
    await perform_nand(clk, s, 2, 2, 0)
    s.o.nand_snoop.assert_eq("true") # inverted


    # Same procedure but write 1
    await perform_nand(clk, s, 0, 0, 2)

    # Then read register 2. Write to register 0 as /dev/null
    await perform_nand(clk, s, 2, 2, 0)
    s.o.nand_snoop.assert_eq("false") # inverted
